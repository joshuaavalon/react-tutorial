[Built Docs](https://joshuaavalon.gitlab.io/react-tutorial/)

1.  Setup
    1. [Node.js](https://nodejs.org/en/)
    2. [VSCode](https://code.visualstudio.com/)
2.  [Packages](https://docs.npmjs.com/misc/semver)
    1.  [Semantic Versioning](https://semver.org/)
3.  [ES6 Features](https://codetower.github.io/es6-features/)
4.  [TypeScript Handbook](https://www.typescriptlang.org/docs/handbook/basic-types.html)
    1. [Type Declaration](https://github.com/DefinitelyTyped/DefinitelyTyped)
5.  React
    1.  [React](https://reactjs.org/docs/introducing-jsx.html)
        1. [Virtual DOM and Internals](https://reactjs.org/docs/faq-internals.html)
        2. [React Hook](https://reactjs.org/docs/hooks-intro.html)
    2.  [Redux](https://redux.js.org/introduction/core-concepts)
    3.  [Reselect](https://github.com/reduxjs/reselect)
    4.  [Router](https://reacttraining.com/react-router/web/example/basic)
6.  [Webpack](https://webpack.js.org/concepts/)
    1.  [Babel](https://babeljs.io/docs/en/)
7.  Lint
    1.  [ESLint](https://eslint.org/)
    2.  [TypeScript ESLint](https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin)
    3.  [Prettier](https://prettier.io/docs/en/install.html)
