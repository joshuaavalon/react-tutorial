# Packages

The default registry is https://registry.npmjs.org/ and the web UI is https://www.npmjs.com/.

## Name

There are two types of packages. Scoped and non-scoped one.

Scoped packages are `@asd/def`. It start will `@` and have `/` in the middle.

Scoped packages can be published by organization or user with their name, you should look out are the packages the ones that you are looking for.

## Version

NPM uses semantic versioning (It does not means all packages follow it but most popular packages follows it.)

The full description is [here][semver]. It is not that long, it is recommended to read thought it once.

Given a version number MAJOR.MINOR.PATCH, increment the:

1. MAJOR version when you make incompatible API changes,
2. MINOR version when you add functionality in a backwards compatible manner, and
3. PATCH version when you make backwards compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.

### npm install

When you install a new pacakge, by default, it will install the latest version.

In `package.json`, it will be something like, `^1.2.3`. `^version` means "Compatible with version".

For example, when you run npm install with `package.json` with `abc` `^1.2.3`, but the latest version of `abc` is `1.2.4`, it will install `1.2.4` because it is compatible.

## package-lock.json

This file contains where the packages are from (registry) and the exact version. It will be updated every time `npm install` is run.

## node_modules

`node_modules` contains the installed packages.

If the package is `asd`, it will be under `node_modules/asd`.

If the package is `@asd/def`, it will be under `node_modules/@asd/def`.

[semver]: https://semver.org/
