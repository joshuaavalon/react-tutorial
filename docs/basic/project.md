# Project

## Hello World

Create `hello.js` with the following content.

```js
console.log("Hello World");
```

Run

```sh
node hello.js
```

and it will print

```
Hello World
```

## Create a new project

Go to a directory and run

```sh
npm init -y
```

It will initialize a Node.js project with all default options. In fact, it just creates a package.json.

::: tip
If you are using VSCode, you can `View > Terminal` or `` Ctrl + ` `` to toggle terminal.
:::

::: tip
`node` is the executable that run JavaScript files.

`npm` is the package manager that come with Node.js.

There are different package managers out there but you should stay with one for the project.
:::

## Install packages

You can install packages in `dependencies` or `devDependencies`.

You should install in

- `dependencies` if the packages are used in runtime.
- `devDependencies` if the packages are used in development only.

Although, it does not matter in our project, but we should follow the best practice.

```sh
npm i <package> <package>
npm install <package> <package>
npm i -D <package> <package>
npm install --dev <package> <package>
```

You can use `-D` or `--save-dev` to install as `devDependencies`.

After you install packages, it will create `package-lock.json` and `node_modules`.

`package-lock.json` contains the details of the installed packages.

`node_modules` contains the installed packages.

## Uninstall packages

```sh
npm uninstall <package>
```

[arrow-functions]: https://hacks.mozilla.org/2015/06/es6-in-depth-arrow-functions/
