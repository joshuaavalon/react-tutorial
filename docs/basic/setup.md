# Setup

## Node.js

Node.js is required to run JavaScript outside of browser. You can download it from [official website][nodejs].

It is recommended to install the latest LTS version instead of latest version.

## Visual Studio Code

It is the recommended editor. You can achieve many functions (e.g. linting, formating) by extensions.
The configuration can also be shared via a JSON file.

But you can any text editor you want, because all the commands run are just command-line commands.

You can download it from [official website][vscode].

[nodejs]: https://nodejs.org/
[vscode]: https://code.visualstudio.com/
