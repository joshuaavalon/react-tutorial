# ES6

Many React tutorial including the official one use ES6 features, you should know about them before starting.

::: tip
You do not need to worry about ES6 is not supported by the browsers because they will compile down to version you needed, just like React itself is not runnable in browsers.
:::

## Arrow Functions

```js
var odds = evens.map(v => v + 1);
var fun = v => v + 1;
```

It is a short hand notation for `function` but they are **NOT** the same.
It is different on how they handle `this`.

```js
var object = {
  name: "Name",
  arrowGetName: () => this.name,
  regularGetName: function() {
    return this.name;
  },
  arrowGetThis: () => this,
  regularGetThis: function() {
    return this;
  }
};

console.log(this.name);
console.log(object.arrowGetName());
console.log(object.arrowGetThis());
console.log(this);
console.log(object.regularGetName());
console.log(object.regularGetThis());
```

```
undefined
undefined
{}
{}
Name
{
  name: 'Name',
  arrowGetName: [Function: arrowGetName],
  regularGetName: [Function: regularGetName],
  arrowGetThis: [Function: arrowGetThis],
  regularGetThis: [Function: regularGetThis]
}
```

You can read more on [ES6 In Depth: Arrow functions][arrow-functions].

On the other hand, if you do not use `this`, they are the same.

## Classes

Yes, there are classes in JavaScript but it is just syntactic sugar on top of prototype-inheritence.

```js
class SkinnedMesh extends THREE.Mesh {
  constructor(geometry, materials) {
    super(geometry, materials);

    this.idMatrix = SkinnedMesh.defaultMatrix();
    this.bones = [];
    this.boneMatrices = [];
    //...
  }
  update(camera) {
    //...
    super.update();
  }
  get boneCount() {
    return this.bones.length;
  }
  set matrixType(matrixType) {
    this.idMatrix = SkinnedMesh[matrixType]();
  }
  static defaultMatrix() {
    return new THREE.Matrix4();
  }
}
```

## String interpolation

```js
var string1 = "One";
var string2 = "Two";
var foo1 = string1 + " / " + string2;
var foo2 = `${string1} / ${string2}`;

var multiLine = `This

Line

Spans Multiple

Lines`;
```

## Destructuring

```js
var [a, , b] = [1, 2, 3];
console.log(a); // 1
console.log(b); // 3

var data = { op: "a", lhs: "b", rhs: "c" };
var { op: d, lhs, rhs } = data; // Rename op to d
console.log(d); // a
console.log(lhs); // b
console.log(rhs); // c

function foo({ op }) {
  // It can be used in parameter position.
  console.log(op);
}
foo(data); // a
```

## Default

```js
var [a = 1] = [];
var { b = 2 } = {};
function f(x, y = 12) {
  return x + y;
}
```

## Spread

```js
var [a, ...b] = [1, 2, 3];
console.log(a); // 1
console.log(b); // [2, 3]

var { c, ...d } = { c: 4, d: 5, e: 6 };
console.log(c); // 4
console.log(d); // { d: 5, e: 6 }

function f(x, y, z) {
  return x + y + z;
}
console.log(f(...[1, 2, 3])); // 6

var parts = ["shoulders", "knees"];
var lyrics = ["head", ...parts, "and", "toes"];

function demo(part1, ...part2) {
  return { part1, part2 };
}
console.log(demo(1, 2, 3, 4, 5, 6)); // {"part1":1,"part2":[2,3,4,5,6]}
```

## let / const

You **SHOULD** always use `let` or `const` over `var`.

The problems of `var` are

- Globally scoped or function scoped
- Can be re-declared
- Hoisting and initialized with `undefined`

### Scoped

```js
var greeter = "hey hi";
var times = 4;

if (times > 3) {
  var greeter = "say Hello instead";
}

console.log(greeter); //"say Hello instead"
```

You would expect `greeter` not accessible outside if but it is accessible.

Both `let` and `const` are block scoped, so it will works as you expected.

### Re-declared

```js
var greeter = "hey hi";
var greeter = "say Hello instead";
greeter = "say Hay instead";
```

This works without any errors.

```js
let greeter = "hey hi";
let greeter = "say Hello instead"; // Error
```

```js
let greeter = "hey hi";
greeter = "say Hello instead"; // Works
```

`let` allows you to update the value but not re-declared.

```js
const greeter = "hey hi";
const greeter = "say Hello instead"; // Error
```

```js
const greeter = "hey hi";
greeter = "say Hello instead"; // Error
```

`const` does not allows you to update or re-declare the value .

### Hoisting

```js
console.log(greeter);
var greeter = "say hello";
```

is interpreted as this

```js
var greeter;
console.log(greeter); //greeter is undefined
greeter = "say hello";
```

However, `let` and `const` will also move to top but it will **NOT** initialized with `undefined`. It will throws error instead.

## for...of

```js
const data = [1, 2, 3];
for (const value of data) {
  console.log(value); // 1
}
```

## Promise

Promise is used to handle async actions.

```js
const p1 = Promise.resolve(1);
const p2 = Promise.reject(new Error());
const p3 = new Promise((resolve, reject) => {
  setTimeout(() => resolve("1"), 10);
});
```

Basically, `resolve` means return and `reject` means throws error.

```js
const p = new Promise((resolve, reject) => {
  setTimeout(() => resolve("1"), 10);
});
p.then(value => console.log(value)).catch(error =>
  console.log(`Failed: ${error}`)
);
```

However, this can become very complex when you nested many callback.
You can use `async` and `await`.

```js
async function foo1() {
  return 1;
}
const foo2 = async () => 1;
const foo3 = async () => Promise.resolve(1);
```

::: tip
JavaScript does not support nested Promise.

Even if you return a Promise in a Promise or return a Promise in a async function,
it will be deconstructed to single layer.
:::

```js
const foo1 = async () => 1;
const foo2 = async () => await foo1();
const foo3 = async () => foo1();

const foo4 = async () => {
  const f2 = await foo2();
  console.log(f2); // 1
  const f3 = await foo3();
  console.log(f3); // 1

  try {
    const f4 = await foo3(); // Use try/catch with await over catch
  } catch {}
};
```

::: tip
There are still some features not covered here but these included all the commonly used ones.
:::
