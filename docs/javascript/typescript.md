# TypeScript

TypeScript is a typed superset of JavaScript that compiles to plain JavaScript.

It try to solve the difficult of developing large JavaScript application. It provides type hints when developing and checks the typings at **COMPILE** time.

## Setup

```
npm i -D typescript
```

You add the following to `scripts` in `package.json`.

```json
{
  "build": "tsc"
}
```

::: tip
Add command to `scripts` allows you to run `npm run <script>` with libraries installed.

Because we have install TypeScript locally, we must run though `npm run`.
:::

## Hello World

Create `hello.ts` with the following content.

```ts
console.log("Hello World");
```

You can run `npm run build -- hello.ts`. It creates `hello.js`.

`--` means passing the arguments to the script. It convert to `tsc hello.ts`. If you pass arguments directly, it may intercepts by `npm run`.

## Type Annotations

```ts
function greet(name: string): void {
  console.log(`Hello ${name}`);
}

greet("Alex");
greet(1); // Argument of type '1' is not assignable to parameter of type 'string'.
```

There are two types of type annotations: implicit and explicit.

```ts
// Implicit

let a = 1; // Implicit type is number.
let b = "a"; // Implicit type is string.

// Explicit
let c: 1 = 1; // Explicit type is 1. You cannot assign value other than 1.
```

## Handbook

It is recommended to read through TypeScript [handbook][handbook].

[handbook]: https://www.typescriptlang.org/docs/handbook
