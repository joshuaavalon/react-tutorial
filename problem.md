# Past Problems

## React Hook

```ts
export const Foo = (): JSX.Element => {
  // You can use hook here
  return <div />;
};

export const foo = (): JSX.Element => {
  // You cannot use hook here because component must be title case. This is a function
  return <div />;
};

export const useFoo = (): void => {
  // You can use hook here
};

export const mapValues = (): void => {
  // You cannot use hook here because it is not a hook. Hook must start with use.
};

useEffect(() => {
  // You cannot use hook here because it is not a hook but it is a callback.
}, []);
```
